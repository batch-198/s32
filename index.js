let http = require("http");

// mock collection of courses:

let courses = [

	{
		name: "Python 101",
		description: "Learn Python",
		price: 25000
	},
	{
		name: "ReactJS 101",
		description: "Learn React",
		price: 35000
	},
	{
		name: "ExpressJS 101",
		description: "Learn ExpressJS",
		price: 28000
	}
];



http.createServer(function(request,response){

	console.log(request.url); //differentiate requests via endpoints
	console.log(request.method); //differentiate request with HTTP method
	/*
		HTTP Requests are differentiated not only via endpoints but also with their methods.

		HTTP Methods simply tells the server what action it must take or what kind of response is needed for the request.

		With an HTTP methods we can create routes with the same endpoint but with different methods.
	*/

	if(request.url === "/" && request.method === "GET"){

		response.writeHead(200,{'Content-Type':'text/plain'});
		response.end("This is the / endpoint. GET method request");

	} else if (request.url === '/' && request.method === "POST"){

		response.writeHead(200,{'Content-Type':'text/plain'});
		response.end("This is the / endpoint. POST method request");
	} else if (request.url === "/" && request.method === "PUT"){

		response.writeHead(200,{'Content-Type':'text/plain'});
		response.end("This is the / endpoint. PUT method request");

	} else if (request.url === "/" && request.method === "DELETE"){

		response.writeHead(200,{'Content-Type':'text/plain'});
		response.end("This is the / endpoint. DELETE method request");
	} else if (request.url === "/courses" && request.method === "GET"){

		response.writeHead(200,{'Content-Type':'application/json'});
		// when sending a JSON format data as response, change the 'content-type' of the response to 'application/json'

		response.end(JSON.stringify(courses));
			//we have to stringify our courses array to send it as a response (because we must only send string)
	} else if (request.url === "/courses" && request.method == "POST"){
		/*Route to add a new course, we have to receive an input from the client

		to be able to receive the request body or the input from the request, we have to add a way to receive that input.

		In NodeJS, this is done in 2 steps:

		requestBody will be a placeholder to contain the data (request body) passed from the client*/
		let requestBody = "";

		/*
		1st step in receiving data from the request in NodeJS is called the data step.

		Data step - will read incoming stream of data from the client and process it so we can save it in the requestBody variable
		*/

		request.on('data',function(data){
			// console.log(data); // stream of data from client

			requestBody+=data;
			// data stream is saved into the variable as a string
		})
		
		// end step - this will run once or after the request data has been completely sent from the client

		request.on('end',function(){
			// we have completely received the data from the client and thus request body now contains our input
			// console.log(requestBody);

			//Initially, requestBody is in JSON format. We cannot add this to our courses array because it's a string. So we have to update requestBody variable with parsed version of the received JSON format data

			requestBody = JSON.parse(requestBody);
			console.log(requestBody);

			// Add the requestBody in the array
			courses.push(requestBody);

			//Check if the courses array if we were able to our requestBody
			// console.log(courses);
		})

		response.writeHead(200,{'Content-Type':'application/json'});
		response.end(JSON.stringify(courses));
	}


}).listen(4000);

console.log("Server running at localhost:4000");
